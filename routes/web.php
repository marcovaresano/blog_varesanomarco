<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\ArticleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'homepage'])->name("homepage");

Route::get('/article/form', [ArticleController::class, 'form'])->name("article.form");

Route::post('/article/submit', [ArticleController::class, 'adSubmit'])->name("article.submit");

Route::get('/article/{article}', [ArticleController::class, 'adDetail'])->name("article");