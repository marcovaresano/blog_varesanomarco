<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|min:3',
            'author'=>'required',
            'description'=>'required|max:144',
            'img' => 'required|image',
        ];
    }

    public function messages() {
        return [
            'title.required' => "Titolo dell'articolo non inserito!",
            'author.required' => "Autore non inserito! Cane Bastardo",
            'description.required' => "Il contenuto dell'articolo non esiste",
            'description.max' => "Il contenuto deve essere massimo di 144 caratteri :)",
            'title.min' => "Il titolo deve essere di almeno 3 caratteri",
            'img.required' => "Inserisci almeno un immagine",
            'img.image' => "Devi inserire un file jpg o png o webp"
        ];
    }
}

