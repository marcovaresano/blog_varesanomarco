<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Requests\ArticleRequest;
// use App\Http\Controllers\ArticleController;

class ArticleController extends Controller
{
   public function adSubmit(ArticleRequest $request) {
    // $article = new Article();
    // $article->title=$request->input("title");
    // $article->author=$request->input("author");
    // $article->description=$request->input("description");
    // $article->img=$request->input("img");   
    // $article->save();

    //metodo mass assignment 
    $article = Article::create( 
        [
            'title'=>$request->input('title'),
            'description'=>$request->input('description'),
            'author'=>$request->input('author'),
            'img' =>$request->file('img')->store('public/img'),
        ]

    );  

    return redirect(route('homepage'))->with('message', 'il tuo articolo è stato pubblicato');
   }

   public function form() {
       return view('form');
   }

   public function adDetail($article) {
       $articolo = Article::find($article);
       return view('article', compact('articolo'));
   }
}
