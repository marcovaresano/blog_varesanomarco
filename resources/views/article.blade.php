<x-layout>
    <div class="card">
        <div class="card-body">
          <h5 class="card-title">{{$articolo->title}}</h5>
          <h6 class="card-text">{{$articolo->author}}</h6>
          <p class="card-text">{{$articolo->description}}</p>
          {{-- <img src="{{$articolo->img}}" alt=""> --}}
        </div>
      </div>
</x-layout>