<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                @if($errors ->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>                            
                        @endforeach
                    </ul>
                </div>
                @endif
                <form method="POST" action="{{route("article.submit")}}" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">Titolo Articolo</label>
                      <input name="title" type="text" value="{{old('title')}}" class="form-control">
                    </div>
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label">Autore</label>
                      <input name="author" type="text" value="{{old('author')}}" class="form-control">
                    </div>

                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Descrizione</label>
                        <textarea name="description" type="text" class="form-control">
                            {{old('description')}}
                        </textarea>
                    </div>

                    <div class="mb-3">
                    <input type="file" name="img" class="btn btn-primary"></input>
                    </div>

                    <button type="submit" class="btn btn-primary">Aggiungi Articolo</button>
                  </form>
            </div>
        </div>
    </div>
</x-layout>