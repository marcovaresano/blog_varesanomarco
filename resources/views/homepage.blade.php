<x-layout>
  @if(session('message'))
  <div class="alert alert-success">
    {{ session('message') }}
  </div>
  @endif
    <div class="container">
        <div class="row">
            @foreach ($articles as $article)
            <div class="col-12 col-md-4">
                <div class="card mt-3">
                    <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                      <img
                        src="{{Storage::url($article->img)}}"
                        class="img-fluid"
                      />
                      <a href="#!">
                        <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                      </a>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">{{$article->title}}</h5>
                        <h6 class="card-title">{{$article->author}}</h6>
                        <p class="card-text">{{$article->description}}</p>
                      <a href="{{route("article", compact('article'))}}" class="btn btn-primary">Button</a>
                    </div>
                  </div>
            </div>
            @endforeach
        </div>
    </div>
</x-layout>


